<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Session extends CI_Session {
    function  __construct() {
        parent::__construct();
    }

    /**
    * Gets all the flashdata stored in CI session
    */
    function all_flashdata() {
        $all_flashdata=$res=array();
        foreach($this->all_userdata() as $k=>$v) {
            if(preg_match('/^flash:old:(.+)/', $k, $res)) {
                $all_flashdata[$res[1]] = $this->userdata($this->flashdata_key.':old:'.$res[1]);
            }
        }
        return $all_flashdata;
    }

}