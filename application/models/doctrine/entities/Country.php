<?php



namespace entities;

/**
 * Country
 *
 * @Table(name="country")
 * @Entity
 */
class Country
{
  /**
   * @var smallint $id
   *
   * @Column(name="id", type="smallint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $countryName
   *
   * @Column(name="country_name", type="string", length=150, nullable=false)
   */
  private $countryName;


  /**
   * Get id
   *
   * @return boolean 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set countryName
   *
   * @param string $countryName
   */
  public function setCountryName($countryName)
  {
    $this->countryName = $countryName;
  }

  /**
   * Get countryName
   *
   * @return string 
   */
  public function getCountryName()
  {
    return $this->countryName;
  }
}