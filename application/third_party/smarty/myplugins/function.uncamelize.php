<?php
/**
 * Smarty plugin
 *
 * @package Smarty
 * @subpackage PluginsFunction
 */

/**
 * Smarty {uncamelize} function plugin
 *
 * Type:     function<br>
 * Name:     uncamelize<br>
 * Date:     Apr 23, 2013<br>
 * Purpose:  returns uncamelized versions of camel case strings<br>
 * Params:
 * <pre>
 * - camel       - the camelized text
 * - splitter    - character with which to split the words
 * </pre>
 * Examples:
 * <pre>
 * {uncamelize camel=$text}
 * {uncamelize camel=$text splitter='-'}
 * {uncamelize camel=$text splitter=' '}
 * </pre>
 *
 * @author Tariqul Islam <tareq at webkutir dot net>
 * @version 1.1
 * @param array                    $params   parameters
 * @param Smarty_Internal_Template $template template object
 * @return string
 */
function smarty_function_uncamelize($params, $template)
{
    if (!isset($params['camel'])) {
        trigger_error("uncamelize: missing 'camel' parameter",E_USER_WARNING);
        return;
    }else{
		$camel = $params['camel'];
	}
	
	if (!isset($params['splitter'])) {
		$splitter = ' ';
	}else{
		$splitter = $params['splitter'];
	}

    $camel=preg_replace('/(?!^)[[:upper:]][[:lower:]]/', '$0', preg_replace('/(?!^)[[:upper:]]+/', $splitter.'$0', $camel));
	
	return ucfirst($camel);
}

?>