<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Country List</title>

    <!-- Bootstrap -->
    <link href="{$systemurl}/assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container" style="padding:25px 0px;">
        <div class="alert alert-success hide">
            <div class="alert-heading">Success</div>
            <p class="notification-message"></p>
        </div>
        
        <div class="alert alert-danger hide">
            <div class="alert-heading">Error</div>
            <p class="notification-message"></p>
        </div>
        
        <button class="btn btn-primary" data-toggle="modal" data-target="#add_box" style="margin-bottom: 25px;">Add New</button>
    	{if count($countries)>0}
    	    <div class="table-responsive">
        	    <table class="table table-striped table-bordered table-hover">
            	    <tr>
            	        <th>ID</th>
            	        <th colspan="2">Country Name</th>
            	    </tr>
    	    {foreach from=$countries item=country}
    	        <tr>
    	            <td>{$country->getId()}</td>
    	            <td>{$country->getCountryName()}</td>
    	            <td><button class="btn btn-danger delCountry" rel="{$country->getId()}">Delete</button></td>
    	        </tr>
    	    {/foreach}
        	    </table>
    	    </div>
    	{else}
    	    <p>No Records Found.</p>
    	{/if}
    </div>
    
    <div class="modal fade" id="add_box" tabindex="-1" role="dialog" aria-labelledby="myAddBox" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myAddBox">Add Country</h4>
                </div>
            
                <form id="addForm" name="addForm" method="post" action="country/insert">
                    <div class="modal-body">
                        <p>
                            <label>Country Name</label>
                            <input type="text" id="countryName" name="countryName" autocomplete="off" />
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-success">Submit</button>
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{$systemurl}/assets/js/jquery-1.11.1.min.js"></script>
    <script src="{$systemurl}/assets/js/bootstrap.min.js"></script>
    <script type="text/javascript">
			$(document).ready(function(){
				var body = $("body");

				body.find(".delCountry").click(function(){
					var _this=$(this), id=_this.attr("rel");

					$.ajax({
						dataType: "json",
						type: "POST",
						url: "country/delete/"+id,
						success: function(data){
							body.find(".alert").addClass("hide");
							if(data.result=='error'){
							    body.find(".alert-danger").find(".notification-message").html(data.msg);
							    body.find(".alert-danger").removeClass("hide");
							}else{
								body.find(".alert-success").find(".notification-message").html(data.msg);
								body.find(".alert-success").removeClass("hide");
								_this.parent().parent().remove();
							}
						}
					});
				});
			});
		</script>
  </body>
</html>