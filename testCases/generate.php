<?php

//echo 'with php'; die();

require_once dirname(__FILE__) . '/../vpu/CIUnit/bootstrap_phpunit.php';
include_once dirname(__FILE__) . '/getops.php';

class Generate
{
	function __construct()
	{
		$this->CI = &set_controller('welcome');
		$this->CI->db = $this->CI->load->database('default_test', TRUE);
	}

	function get_table_data($table, $limit = 5)
	{
		$sql = "SELECT * FROM `$table`";
		if ($limit > 0)
		{
			$sql .= " LIMIT " . (int) $limit;
		}
		$query = $this->CI->db->query($sql);
		$table_fields = $this->CI->db->field_data($table);
		$data = Array();
		foreach($query->result_array() as $row)
		{
			$i = 0;
			foreach($row as $field => $val)
			{
				if(!in_array($table_fields[$i]->type, array('int', 'bigint', 'tinyint', 'smallint', 'mediumint', 'decimal', 'float', 'double', 'real', 'bit', 'boolean', 'serial')))
				{
					$row[$field] = '"' . addSlashes($val) . '"';
				}
				$i++;
			}
			$data[(count($data) + 1)] = $row;
		}
		return $data;
	}

	function fixtures($args = Array())
	{
		if (substr($this->CI->db->database, -5, 5) != '_test')
		{
		   die("\nSorry, the name of your test database must end on '_test'.\n".
		   "This prevents deleting important data by accident.\n");
		}
		
		//$this->CI->db->database = preg_replace("#_test$#", "_development", $this->CI->db->database);
		if (!$this->CI->db->db_select())
		{
			die("\nCould not select development database.\n");   
		}		
	   
		$opts = getopts(array(
			'rows'	 => array('switch' => 'n', 'type' => GETOPT_VAL, 'default' => 5),
			'fixtures' => array('switch' => 'f', 'type' => GETOPT_MULTIVAL),
			'output'   => array('switch' => 'o', 'type' => GETOPT_VAL, 'default' => '/fixtures')
		), $args);
		
		
		$rows	 = $opts['rows'];
		$fixtures = $opts['fixtures'];
		$output   = rtrim(str_replace('\\','/',$opts['output']), '/') . '/';
		if (!@chdir(dirname(__FILE__) . '/' . $output))
		{
			die("\nOutput directory '$output' does not exist.\n");   
		}
							   
		$tables = $this->CI->db->list_tables();
		if (count($fixtures) == 0)
		{
			$fixtures = $tables;
		}
		else
		{
			/* check tables */
			foreach ($fixtures as $fixture)
			{
				if (!in_array($fixture, $tables))
				{
					die("\nTable `$fixture` does not exist.\n");
				}   
			}	   
		}
		
		
		foreach ($fixtures as $fixture)
		{
			$filename = 'fixtures/'.$fixture . '_fixt.yml';
			if (file_exists(__DIR__ . '/' . $filename))
			{
				echo " fixture $fixture already exits!\n";
			}
			else
			{
				$data = $this->get_table_data($fixture, $rows);
				$yaml_data = CIUnit::$spyc->dump($data);
				
				$yaml_data = preg_replace('#^\-\-\-#', '', $yaml_data);
				
				/* don't check if the file already exists */
				file_put_contents(__DIR__ . '/' . $filename, $yaml_data);
				echo " *** fixture $fixture written.\n";
			}	
		}
	}
}

$args = $_SERVER['argv'];
$self = array_shift($args);

$generate = new Generate;
$generate_what = array_shift($args);

if (!method_exists($generate, $generate_what))
{
	die("\nMethod '$generate_what' is invalid.
Usage:
	php generate.php fixtures <options>
Options:
	-f  tables of which fixtures should be created (-f table1 -f table2 etc)
		 omitting the -f option, selects all tables in the database.
	-n  number of rows in fixtures <default: 5>
	-o  output directory\n");
}
else
{
	$generate->$generate_what($_SERVER['argv']);   
}
