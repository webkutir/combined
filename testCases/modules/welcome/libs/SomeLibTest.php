<?php

/**
 * @group modules
 * @group Welcome
 * @group Library
 */

class SomeLibTest extends CIUnit_TestCase
{
	public function setUp()
	{
		// Set up fixtures to be run before each test
		
		// Load the tested library so it will be available in all tests
		//$this->CI->load->library('example_lib', '', mylib);
	}
	
	public function testMethod()
	{
		// Check if everything is ok
		//$this->assertEquals(4, $this->CI->mylib->add(2, 2));
	}
	
	public function testIncompleteMethod()
	{
	    $this->markTestIncomplete('This is just for testing Incomplete Test');
	}
	
	public function testSkippedMethod()
	{
	    $this->markTestSkipped("This is just for testing Skipped Test");
	}
	
	public function testFailingIssues()
	{
	    $this->assertEquals(5, 6, "Testing Falied Tests");
	}
}
