<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * File extensions
 *
 * breaks a file name into it's components
 * example input: document.doc
 * output: array( "filename" => "document", "extension" => ".doc", "extension_undotted" => "doc")
 *
 * @access  public
 * @param string
 * @return  array
 */
function _file_extension($filename){
	$x = explode('.', $filename);
	$ext=end($x);
	$filenameSansExt=str_replace('.'.$ext,"",$filename);
	return array(
			"filename"=>$filenameSansExt,
			"extension"=>'.'.$ext,
			"extension_undotted"=>$ext
	);
}

/**
 * Uncamelize
 *
 * returns uncamelized versions of camel case strings
 * example input: theQuickBrownFox
 * output: The Quick Brown Fox
 *
 * @access	public
 * @param string $camel    Camelized Text
 * @param string $splitter character by which final string's words will be seperated
 * 
 * @return	string
 */
function _uncamelize($camel,$splitter=" ") {
	$camel=preg_replace('/(?!^)[[:upper:]][[:lower:]]/', '$0', preg_replace('/(?!^)[[:upper:]]+/', $splitter.'$0', $camel));
	return ucfirst($camel);
}
