<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Orm extends CI_Controller {
	
	private $st;
	private $cmf;
	
	public function __construct(){
		parent::__construct();
		
		$this->st = new \Doctrine\ORM\Tools\SchemaTool($this->doctrine->em);
		$this->cmf = $this->doctrine->em->getMetadataFactory();
		$this->cmf->setEntityManager($this->doctrine->em);
		
		$this->module = 'orm';
		$this->controller = 'orm';
	}
	
	public function index(){
		$this->parser->parse("orm");
	}
	
	public function createSchema($type='return'){
		try{
			$this->st->createSchema($this->cmf->getAllMetadata());
			
			if($type=='ajax'){
				echo json_encode(array("type"=>"success","msg"=>"Schema created successfuly."));
			}else{
				echo "Schema created successfuly.";
			}
		}catch(Exception $ex){
			if($type=='ajax'){
				echo json_encode(array("type"=>"error","msg"=>"Create Schema Operation Failed. Please check your Entities.","detail"=>$ex->getMessage()));
			}else{
				echo "Create Schema Operation Failed. Please check your Entities. Details: <pre>".var_dump($ex)."</pre>";
			}
		}
	}

	public function dropSchema($type='return'){
		try{
			$this->st->dropSchema($this->cmf->getAllMetadata());
			
			if($type=='ajax'){
				echo json_encode(array("type"=>"success","msg"=>"Schema droped successfuly."));
			}else{
				echo "Schema droped successfuly.";
			}
		}catch(Exception $ex){
			if($type=='ajax'){
				echo json_encode(array("type"=>"error","msg"=>"Drop Schema Operation Failed. Please try again.","detail"=>$ex->getMessage()));
			}else{
				echo "Drop Schema Operation Failed. Please try again.";
			}
		}
	}

	public function dropDatabase($type='return'){
		try{
			$this->st->dropDatabase();
			
			if($type=='ajax'){
				echo json_encode(array("type"=>"success","msg"=>"Database droped successfuly."));
			}else{
				echo "Database droped successfuly.";
			}
		}catch(Exception $ex){
			if($type=='ajax'){
				echo json_encode(array("type"=>"error","msg"=>"Drop Database Operation Failed. Please try again.","detail"=>$ex->getMessage()));
			}else{
				echo "Drop Database Operation Failed. Please try again.";
			}
		}
	}

	public function updateSchema($type='return',$option=''){
		try{
			if($option=='force'){
				$saveMode=true;
			}else{
				$saveMode=false;
			}
			$this->st->updateSchema($this->cmf->getAllMetadata(), $saveMode);
			
			if($type=='ajax'){
				echo json_encode(array("type"=>"success","msg"=>"Schema updated successfuly."));
			}else{
				echo "Schema updated successfuly.";
			}
		}catch(Exception $ex){
			if($type=='ajax'){
				echo json_encode(array("type"=>"error","msg"=>"Update Schema Operation Failed. Please check your Entities.","detail"=>$ex->getMessage()));
			}else{
				echo "Update Schema Operation Failed. Please check your Entities. Details:".var_dump($ex); //$ex->getTrace()
			}
		}
	}
	
	public function generateProxy($type='return'){
		try{
			// doctrine orm:generate-proxies ./Proxies
		}catch(Exception $ex){
			if($type=='ajax'){
				echo json_encode(array("type"=>"error","msg"=>"Generate Proxies Operation Failed. Please check your Entities.","detail"=>$ex->getMessage()));
			}else{
				echo "Generate Proxies Operation Failed. Please check your Entities.";
			}
		}
	}
	
	/**
	* generate entity objects automatically from mysql db tables
  * @return none
  */
  function generateEntity($type='return'){
  	try{
  		$this->doctrine->em->getConnection()->getDatabasePlatform()->registerDoctrineTypeMapping('set', 'string');
			$this->doctrine->em->getConnection()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
			$driver = new \Doctrine\ORM\Mapping\Driver\DatabaseDriver($this->doctrine->em->getConnection()->getSchemaManager());
			$this->doctrine->em->getConfiguration()->setMetadataDriverImpl($driver);
			$this->cmf = new \Doctrine\ORM\Tools\DisconnectedClassMetadataFactory($this->doctrine->em);
			$this->cmf->setEntityManager($this->doctrine->em); 
			$metadata = $this->cmf->getAllMetadata(); 
			$generator = new \Doctrine\ORM\Tools\EntityGenerator();
			$generator->setUpdateEntityIfExists(true);
			$generator->setRegenerateEntityIfExists(true);
			$generator->setBackupExisting(false);
			$generator->setGenerateStubMethods(true);
			$generator->setGenerateAnnotations(true);
			$generator->setNumSpaces(2);
			$generator->generate($metadata, APPPATH."cache/doctrine/generated_entities");
  		
			if($type=='ajax'){
				echo json_encode(array("type"=>"success","msg"=>"Entities Generated successfuly."));
			}else{
				echo "Entities Generated successfuly.";
			}
  	}catch(Exception $ex){
  		if($type=='ajax'){
  			echo json_encode(array("type"=>"error","msg"=>"Generate Entity Operation Failed. Please try again.","detail"=>$ex->getMessage()));
  		}else{
  			echo "Generate Entity Operation Failed. Please try again.";
  		}
  	}
  }
}