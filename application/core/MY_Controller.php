<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * MY_Controller
 *
 * @category MY_Controller
 * @package  CodeIgniter
 * @author   Tariqul Islam <tareq@webkutir.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://nibssolutions.com
 */
class MY_Controller extends MX_Controller
{
    protected $data=array();
    public $em;

    /**
     * Constructor of MY Controller
     */
    function __construct()
    {
        parent::__construct();
        $this->em=$this->doctrine->em;
    }

    /**
     * _remap
     * 
     * This Function will be called before anyother function is called
     * in Codeigniter. This function will check for the method existance
     * and show error message if the mothod is not found. Also this method
     * will create Constants from 'General Settings' Table value which will
     * be used throughout the system.
     * 
     * @param string $method method name
     * @param array  $params array of the parameters
     * 
     * @return mixed as per method
     */
    function _remap($method, $params = array())
    {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        }

        $this->parser->parse('page_not_found', $this->data);
    }
}
