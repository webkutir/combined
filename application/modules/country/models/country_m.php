<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Country Model
 *
 * @category Model
 * @package  CodeIgniter
 * @author   Tariqul Islam <tareq@webkutir.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://nibssolutions.com
 */
class Country_m extends CI_Model
{
    /**
     * Constructor of MY Model
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Counts all the rows in a specified table
     * 
     * @return number
     */
    function count()
    {
        return $this->db->from('country')->count_all_results();
    }
}
