<?php

/**
 * @group Model
 */

class CountryModelTest extends CIUnit_TestCase
{
	protected $tables = array(
		'country' 			=> 'country'
	);
	
	public function __construct($name = NULL, array $data = array(), $dataName = '')
	{
		parent::__construct($name, $data, $dataName);
	}
	
	public function setUp()
	{
		parent::tearDown();
		parent::setUp();
		
		$this->CI->load->model('country/country_m', 'country');
		$this->country=$this->CI->country;
	}

	public function testCountTableRows()
	{
		$this->assertEquals(5, $this->country->count());
	}
}
