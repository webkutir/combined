<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Country extends MY_Controller {
    
    function __construct() {
        parent::__construct();
        $this->parser->setModule("country");
    }

	public function index()
	{
	    $repository = $this->em->getRepository('entities\Country');
	    $list = $repository->findAll();

		$this->parser->parse("country", array("countries"=>$list));
	}
	
	public function delete($id)
	{
	    $country = $this->em->find('entities\Country', $id);
	    if(!is_null($country)){
	        //Delete It Now
	        $this->em->remove($country);
	        $this->em->flush();
	        
	        echo json_encode(array("result"=>"success", "msg"=>"Country deleted successfully."));
	    }else{
	        echo json_encode(array("result"=>"error", "msg"=>"Country with ID: ".$id." is not found."));
	    }
	}
	
	public function insert()
	{
	    $data = $this->input->post(null, true);
	    
	    if($data['countryName']!=''){
    	    $country = new entities\Country();
    	    $country->setCountryName($data['countryName']);
    	    
    	    $this->em->persist($country);
    	    $this->em->flush();
	    }
	    
	    redirect("country", 301);
	}
}

/* End of file country.php */
/* Location: ./application/controllers/country.php */