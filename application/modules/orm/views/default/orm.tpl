<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ORM Controller</title>

    <!-- Bootstrap -->
    <link href="{$systemurl}/assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container" style="padding:25px 0px;">
    	<div class="row">
      	<div class="col-md-2">
        	<button class="btn btn-primary" id="createSchema">Create Schema</button>
        </div>
        <div class="col-md-2">
        	<button class="btn btn-danger" id="dropSchema">Drop Schema</button>
        </div>
        <div class="col-md-2">
        	<button class="btn btn-warning" id="updateSchema">Update Schema</button>
        </div>
        <div class="col-md-2">
        	<button class="btn btn-danger" id="dropDatabase">Drop Database</button>
        </div>
        <div class="col-md-2">
        	<button class="btn btn-info" id="generateProxy">Generate Proxy</button>
        </div>
        <div class="col-md-2">
        	<button class="btn btn-info" id="generateEntity">Generate Entity</button>
        </div>
      </div>
      <hr>
      <div class="row" id="resultWindow">
      	&nbsp;
      </div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{$systemurl}/assets/js/jquery-1.11.1.min.js"></script>
    <script src="{$systemurl}/assets/js/bootstrap.min.js"></script>
    <script language="javascript">
			$(document).ready(function(){
				var body = $("body");
				
				body.find("#createSchema,#dropSchema,#updateSchema,#dropDatabase,#generateProxy,#generateEntity").click(function(e){
					var _this=$(this), id=_this.attr("id");
					$.ajax({
						dataType: "json",
						type: "POST",
						url: "orm/"+id+"/ajax",
						success: function(data){
							var html = '';
							if(data.type=='error'){
								html = '<div class="alert alert-danger">';
								html += '<h4 class="alert-heading">Error</h4>';
								html += '<p class="notification-message">'+data.msg+'</p>';
								html += '</div>';
								html += '<div style="line-height: 25px;text-align: justify;margin-top: 25px;padding: 10px;">';
								html += data.detail;
								html += '</div>';
							}else{
								html = '<div class="alert alert-success">';
								html += '<h4 class="alert-heading">Success</h4>';
								html += '<p class="notification-message">'+data.msg+'</p>';
								html += '</div>';
							}
							
							body.find("#resultWindow").html(html);
						}
					});
				});
			});
		</script>
  </body>
</html>