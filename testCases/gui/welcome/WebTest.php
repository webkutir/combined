<?php

/**
 * @group GUI
 * @group Welcome
 */

class WebTest extends CIUnit_TestCase2_Selenium
{
    protected function setUp()
    {
    	$this->setBrowser('firefox');
        $this->setBrowserUrl('http://www.example.com/');
        $this->setHost('localhost');
        $this->setPort(4443);
    }

    public function testTitle()
    {
        $this->url('http://www.example.com/');
        $this->assertEquals('Example Title', $this->title());
    }

}