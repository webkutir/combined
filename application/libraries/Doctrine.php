<?php
use Doctrine\ORM\EntityManager,
    Doctrine\ORM\Configuration,
		Doctrine\ORM,
  	Doctrine\Common\Cache\ArrayCache,
  	Doctrine\OXM;
 
define('DEBUGGING', FALSE);
 
class Doctrine {
 
    public $em = null;
 
    public function __construct()
    {
        // load database configuration and custom config from CodeIgniter
        require APPPATH . 'config/database.php';
 
        // Set up class loading.
        require_once APPPATH . 'third_party/Doctrine/Common/ClassLoader.php';
 
        $doctrineClassLoader = new \Doctrine\Common\ClassLoader('Doctrine', APPPATH . 'third_party');
        $doctrineClassLoader->register();
 
        $entitiesClassLoader = new \Doctrine\Common\ClassLoader('entities', APPPATH . 'models/doctrine');
        $entitiesClassLoader->register();
 
        $proxiesClassLoader = new \Doctrine\Common\ClassLoader('Proxies', APPPATH . 'cache/doctrine/proxies');
        $proxiesClassLoader->register();
 
        $symfonyClassLoader = new \Doctrine\Common\ClassLoader('Symfony', APPPATH . 'third_party/Doctrine');
        $symfonyClassLoader->register();
 
        // Choose caching method based on application mode (ENVIRONMENT is defined in /index.php)
        if (ENVIRONMENT == 'development') {
            $cache = new \Doctrine\Common\Cache\ArrayCache;
        } else {
            $cache = new \Doctrine\Common\Cache\ApcCache;
        }
 
        // Set some configuration options
        $config = new Configuration;
 
        // Metadata driver
        $driverImpl = $config->newDefaultAnnotationDriver(APPPATH . 'models/doctrine/entities');
        $config->setMetadataDriverImpl($driverImpl);
 
        // Caching
        $config->setMetadataCacheImpl($cache);
        $config->setQueryCacheImpl($cache);
 
        // Proxies
        $config->setProxyDir(APPPATH . 'cache/doctrine/proxies');
        $config->setProxyNamespace('Project\Proxies');
 
        if (ENVIRONMENT == 'development') {
            $config->setAutoGenerateProxyClasses(TRUE);
        } else {
            $config->setAutoGenerateProxyClasses(FALSE);
        }
 
        // SQL query logger
        if (DEBUGGING)
        {
            $logger = new \Doctrine\DBAL\Logging\EchoSQLLogger;
            $config->setSQLLogger($logger);
        }
 
        // Database connection information
        $connectionOptions = array(
            'driver' => 'pdo_mysql',
            'user' => $db[$active_group]['username'],
            'password' => $db[$active_group]['password'],
            'host' => $db[$active_group]['hostname'],
            'dbname' => $db[$active_group]['database']
        );
 
        // Create EntityManager
        $this->em = EntityManager::create($connectionOptions, $config);
    }
}