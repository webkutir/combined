## Purpose of this Project

Basically its an starter project for most of my projects. In this project I just combined the followings:

1.  CodeIgniter 2.1.4
2.  CodeIgniter HMVC
3.  Smarty 3.1.18
4.  Doctrine ORM 2.3.3
5.  CIUnit &
6.  Visual PHPUnit

I need those tools frequently in my projects. So to help myself and others to start with those tools I 
am publishing this project.

## Project Structure

You will find 5 Tags which are as follows:

1. CodeIgniter
2. HMVC
3. Smarty
4. Doctrine &
5. VisualPHPUnit

In **CodeIgniter** tag only codeigniter is intalled.  
In **HMVC** tag HMVC is implemented with CodeIgniter.  
In **Smarty** tag CodeIgniter HMVC and Smarty is integrated.  
In **Doctrine** tag CodeIgniter HMVC, Smarty & Doctrine are integrated.  
In **VisualPHPUnit** tag CodeIgniter HMVC, Smarty, Doctrine, CIUnit & Visual PHPUnit are integrated.  

## Usage

Please set `0755` permission for `/application/cache` directory.  

It is designed to have multiple templates in one project if you need so. By default **Default** template is activated.
If you need more templates then just create a folder to the name of your template in module's Views directory and put your
files in it. You need to modify Line # 24 of `/application/config/smarty.php` file to activate your new template.

## Information about using Doctrine

You have to create and put all of your entity classes at `/application/models/doctrine/entities` folder. For your conveniyence 
I developed an Web interface to work with Doctrine's most used commands. Browse to `<your domain>/orm` and you will find the following
buttons there:

1.  **Create Schema** -- This will create db schema from your Entity Classes.
2.  **Drop Schema** -- This will drop your db schema
3.  **Update Schema** -- This will update your db Schema
4.  **Drop Database** -- This will drop your whole Database
5.  **Generate Proxy** -- This will generate your proxy classes from your DB Schema and stores the proxy classes to `/application/cache/doctrine/proxies` folder.
6.  **Generate Entity** -- This will generate entity Classes from your DB Schema and stores the entity classes to `/application/cache/doctrine/generated_entities` folder.

I created an module named **country** to show example usage of Doctrine. You can browse at `<your domain>/country` to see it in browser.

## Information about using VisualPHPUnit

I am using CIUnit and VisualPHPUnit for unit testing of CodeIgniter's controllers, models, views, libraries, helpers etc. I have set up
some example test cases at `/testCases` folder. Here all of your test cases will reside. Please be noted that currently we can't view
test results of Selenium Tests in Visual PHPUnit. But we can run those by running `phpunit` command from `/testCases/gui` folder from command prompt.

Point to your browser at `<your domain>/vpu` to see the web interface from where you can run and see your test results.

Please be noted that you will need a database ended with _test suffix to use database tests. Please have a look at `/application/config/database.php` file to see
how we are using the test db.

In `/testCases/fixtures` folder all of your DB fixtures will reside. You can create blank fixtures or contruct fixtures from your exiting table by using the following
command assigned from command prompt.

1. **To Generate blank Fixtures from your DB**  --- run `php generate fixtures` command from `/testCases` folder.
2. **To Generate Fixtures with data from your DB** -- run `php generate.php fixtures` command from `/testCases` folder. This will bring upto 5 rows from your DB tables.
If you need more rows then use `php generate.php fixtures -n <number>` command. Here <number> is your required rows.

We also include an ANT build file in the projects root folder alone with our custome Coding Style XML file.

If you need assistance to use this as your starter project feel free to contact me at `tareq@webkutir.net`.

Happy and Easy Coding.