<?php

/**
 * @group Helper
 */

class GenericHelperTest extends CIUnit_TestCase
{
	public function setUp()
	{
		$this->CI->load->helper('generic_helper');
	}
	
	public function testFileExtension()
	{
		$expected = array("filename" => "document", "extension" => ".doc", "extension_undotted" => "doc");
		$filename = 'document.doc';
		
		$this->assertEquals($expected, _file_extension($filename));
	}
	
	public function testUncamelize()
	{
		$this->assertEquals('The Quick Brown Fox', _uncamelize('theQuickBrownFox'));
	}
}
